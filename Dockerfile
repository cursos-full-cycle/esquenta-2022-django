FROM python:3.10.2-slim

# no container, cria um usuário com permissões restritas
RUN useradd -ms /bin/bash python

# gestor de pacotes, melhor que o pip
RUN pip install pipenv

# altera para o usuário criado
USER python

WORKDIR /home/python/app

ENV PIPENV_VENV_IN_PROJECT=True

# Esse comando é tipo um while True, roda indefinidamente
CMD ["tail","-f","/dev/null"]
