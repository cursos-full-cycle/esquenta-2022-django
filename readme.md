[Tuto Full Cycle](https://www.youtube.com/watch?v=NC6aUo5rxco)

# Rodar o VSCode no container
- instalar a extensão do VSCode `Dev Containers`
- `ctrl + shift + p`  
- seleciona `Dev Containers: Open Folder in Container...`
- seleciona o folder do projeto
- seleciona `From docker-compose.yaml

# Ao entrar no container:
```bash
# instala dependências
pip install

# habilita o venv no ambiente local
pipenv shell

# cria projeto
django-admin startproject cartola_fc

# inicia o banco de dados - refaz sempre q atualizar o db
python manage.py makemigrations
python manage.py migrate

# criar superusuario
python manage.py createsuperuser

# roda o servidor
python manage.py runserver 0.0.0.0:8000
```

- acessa o banco de dados em `localhost:8000/admin`
- o usuário que eu criei é o admin, senha admin