# Generated by Django 4.1.3 on 2022-12-04 21:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_team_player_alter_match_team_a_goal_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='match',
            options={'verbose_name': 'Jogo', 'verbose_name_plural': 'Jogos'},
        ),
        migrations.AlterModelOptions(
            name='myteam',
            options={'verbose_name': 'Meu Time', 'verbose_name_plural': 'Meus Times'},
        ),
        migrations.AlterModelOptions(
            name='player',
            options={'verbose_name': 'Jogador', 'verbose_name_plural': 'Jogadores'},
        ),
        migrations.AlterModelOptions(
            name='team',
            options={'verbose_name': 'Time', 'verbose_name_plural': 'Times'},
        ),
        migrations.AlterField(
            model_name='match',
            name='match_date',
            field=models.DateTimeField(verbose_name='Data'),
        ),
        migrations.AlterField(
            model_name='match',
            name='team_a',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='team_a_matches', to='app.team', verbose_name='Time A'),
        ),
        migrations.AlterField(
            model_name='match',
            name='team_a_goal',
            field=models.IntegerField(default=0, verbose_name='Gols Time A'),
        ),
        migrations.AlterField(
            model_name='match',
            name='team_b',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='team_b_matches', to='app.team', verbose_name='Time B'),
        ),
        migrations.AlterField(
            model_name='match',
            name='team_b_goal',
            field=models.IntegerField(default=0, verbose_name='Gols Time B'),
        ),
        migrations.AlterField(
            model_name='myteam',
            name='name',
            field=models.CharField(max_length=50, verbose_name='Nome'),
        ),
        migrations.AlterField(
            model_name='myteam',
            name='player',
            field=models.ManyToManyField(to='app.player', verbose_name='Jogador'),
        ),
        migrations.AlterField(
            model_name='player',
            name='initial_price',
            field=models.FloatField(verbose_name='Preço Inicial'),
        ),
        migrations.AlterField(
            model_name='player',
            name='name',
            field=models.CharField(max_length=50, verbose_name='Nome'),
        ),
        migrations.AlterField(
            model_name='team',
            name='name',
            field=models.CharField(max_length=50, verbose_name='Nome'),
        ),
        migrations.AlterField(
            model_name='team',
            name='player',
            field=models.ManyToManyField(to='app.player', verbose_name='Jogador'),
        ),
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('minutes', models.IntegerField(verbose_name='Minutos')),
                ('action', models.CharField(choices=[('Gol', 'Goal'), ('Assist', 'Assistência'), ('Yellow Card', 'Cartão Amarelo'), ('Red Card', 'Cartão Vermelho')], max_length=50, verbose_name='Ação')),
                ('match', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.match', verbose_name='Jogo')),
                ('player', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.player', verbose_name='Jogador')),
                ('time', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.team', verbose_name='Time')),
            ],
            options={
                'verbose_name': 'Ação do Jogo',
                'verbose_name_plural': 'Ações do Jogo',
            },
        ),
    ]
