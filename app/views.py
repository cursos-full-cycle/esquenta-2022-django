from django.shortcuts import render
from .models import Player

def show_players(request):
    players = Player.objects.all()
    # busca dentro da pasta app/templates
    return render(request, 'app/players.html', {'players': players}) 