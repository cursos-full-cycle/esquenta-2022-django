from django.db import models

# Django ORM


class Player(models.Model):
    name = models.CharField(max_length=50, verbose_name='Nome')
    initial_price = models.FloatField(verbose_name='Preço Inicial')

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Jogador'
        verbose_name_plural = 'Jogadores'


class Team(models.Model):
    name = models.CharField(max_length=50, verbose_name='Nome')
    player = models.ManyToManyField(Player, verbose_name='Jogador')

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Time'
        verbose_name_plural = 'Times'


class MyTeam(models.Model):
    name = models.CharField(max_length=50, verbose_name='Nome')
    player = models.ManyToManyField(Player, verbose_name='Jogador')

    def __str__(self):
        return [player.__str__() for player in self.player.all()].__str__()
    
    class Meta:
        verbose_name = 'Meu Time'
        verbose_name_plural = 'Meus Times'

class Match(models.Model):
    match_date = models.DateTimeField(verbose_name='Data')
    team_a = models.ForeignKey(
        Team, on_delete=models.PROTECT, related_name='team_a_matches', verbose_name='Time A')
    team_a_goal = models.IntegerField(default=0, verbose_name='Gols Time A')
    team_b = models.ForeignKey(
        Team, on_delete=models.PROTECT, related_name='team_b_matches', verbose_name='Time B')
    team_b_goal = models.IntegerField(default=0, verbose_name='Gols Time B')

    def __str__(self):
        return f'{self.team_a} x {self.team_b}'
    
    class Meta:
        verbose_name = 'Jogo'
        verbose_name_plural = 'Jogos'
        
class Action(models.Model):
        player = models.ForeignKey(Player, on_delete=models.PROTECT, verbose_name='Jogador')
        time = models.ForeignKey(Team, on_delete=models.PROTECT, verbose_name='Time')
        match = models.ForeignKey(Match, on_delete=models.PROTECT, verbose_name='Jogo')
        minutes = models.IntegerField(verbose_name='Minutos')
        
        class Actions(models.TextChoices):
            GOAL = 'Gol', 'Goal'
            ASSIST = 'Assist', 'Assistência'
            YELLOW_CARD = 'Yellow Card', 'Cartão Amarelo'
            RED_CARD = 'Red Card', 'Cartão Vermelho'
            
        action = models.CharField(max_length=50, choices=Actions.choices, verbose_name='Ação')
        
        def __str__(self):
            return f'{self.player} - {self.action}'
        
        class Meta:
            verbose_name = 'Ação do Jogo'
            verbose_name_plural = 'Ações do Jogo'
            
